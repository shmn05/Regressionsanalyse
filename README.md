# Thema Regressionsanalyse

## Sven Hohmann

## Funktionsbeschreibung
- Kurschart
- verschiedene Regressionsarten plotten
- diverse Funktionen zur Analyse

## Bezug zu Mathematik
- Stochastik
- Lineare Algebra 

## Zeitplan
- Grobkonzept Ende Okt
- GUI bis Ende November, Entwicklungsumgebung NetBeans
- Programmierung Regressions-Funktionen
- Fertigstellung März


## Dokumentation
- Basierend auf Javadoc
- Sprache Deutsch
- PAP (nach Bedarf)


## Offene Punkte, Fragen
- Programmierung neue Regressionsarten
